import sys

poly = sum(map(lambda x: 2 ** x, [32, 26, 23, 22, 16, 12, 11, 10, 8, 7, 5, 4, 2, 1, 0]))

def crc32(data):
    bindata = sum(map(lambda (i, x): ord(x) << 7 * i, enumerate(reversed(data))))
    return divmod(bindata, poly)[1]


if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        print hex(crc32(f.read()))