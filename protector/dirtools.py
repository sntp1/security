import config as cfg
import os
from hashlib import sha256
import crypttools as ct
import shutil


def user_dir(username):
    return os.path.join(cfg.PRO_DIR, username)


def ensure_pro_dir():
    if not os.path.exists(cfg.PRO_DIR):
        os.mkdir(cfg.PRO_DIR)


def ensure_user_dir(username):
    p = user_dir(username)
    if not os.path.exists(p):
        os.mkdir(p)


def __files(path):
    return ['%s/%s' % (d[0], f) for d in os.walk(path) for f in d[2]]


def dir_hash(username):
    p = user_dir(username)
    h = sha256()
    for filename in __files(p):
        with open(filename, 'rb') as f:
            h.update(f.read())
    return h.hexdigest()


def check_consistency(username, db_hash):
    return db_hash == dir_hash(username)


def xor_dir(username, pass_hash):
    p = user_dir(username)
    for filename in __files(p):
        ct.xor_file(filename, pass_hash)


def delete(username):
    p = user_dir(username)
    shutil.rmtree(p)


ensure_pro_dir()
