from os import path
import os


PRO_DIR = path.join(os.environ['HOME'], '.PRO')
DATABASE_URL = 'sqlite:///' + path.join(PRO_DIR, 'users.db')