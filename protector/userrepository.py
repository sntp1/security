from sqlalchemy import Column, String
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import config as cfg


Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    username = Column(String(32), primary_key=True)
    password_hash = Column(String(64))
    folder_hash = Column(String(64))


engine = create_engine(cfg.DATABASE_URL)
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()


def user_exists(username):
    return session.query(User).filter(User.username == username).count() > 0


def create_user(username, password_hash, folder_hash):
    user = User(username=username,
                password_hash=password_hash,
                folder_hash=folder_hash)
    session.add(user)
    session.commit()


def check_user(username, pass_hash):
    return session.query(User).filter(User.username == username).filter(User.password_hash == pass_hash).first()


def delete(username):
    user = session.query(User).filter(User.username == username).first()
    session.delete(user)
    session.commit()


def get_user(username):
    return session.query(User).filter(User.username == username).first()


def update_hash(username, folder_hash):
    user = session.query(User).filter(User.username == username).first()
    user.folder_hash = folder_hash
    session.add(user)
    session.commit()