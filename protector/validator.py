import re


def validate_login(login):
    return re.match(r'^\w+$', login) is not None
