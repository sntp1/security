from itertools import izip, cycle


def xor_file(filename, key):
    with open(filename, 'rb') as f:
        data = f.read()
        xored = ''.join(chr(ord(x) ^ ord(y)) for (x, y) in izip(data, cycle(key)))
    with open(filename, 'wb') as f:
        f.write(xored)
