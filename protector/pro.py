import argparse
import sys
import promanager as pm
from getpass import getpass
from validator import validate_login


pro_parser = argparse.ArgumentParser(description='PRO. Protects your files.')
command_parsers = pro_parser.add_subparsers(help='Available commands', dest='command')
register_parser = command_parsers.add_parser('register', help='Register new user')
login_parser = command_parsers.add_parser('login', help='Login')
delete_parser = command_parsers.add_parser('delete', help='Delete user')


if __name__ == '__main__':
    args = pro_parser.parse_args(sys.argv[1:])
    if args.command == 'register':
        login = raw_input("Login: ")
        if not validate_login(login):
            print "Login should only contains symbols: a-zA-Z0-9_"
            quit(1)
        password = getpass("Password: ")
        repeat_password = getpass("Repeat password: ")
        if password != repeat_password:
            print "Passwords don't match"
            quit(1)
        print pm.register(login, password)
    elif args.command == 'login':
        login = raw_input("Login: ")
        password = getpass("Password: ")
        res = pm.login(login, password)
        if res is not None:
            print res
    elif args.command == 'delete':
        login = raw_input("Login: ")
        password = getpass("Password: ")
        print pm.delete(login, password)
