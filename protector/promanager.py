import userrepository as ur
import dirtools
from hashlib import sha256
import os


def __hash(password):
    return sha256(password).hexdigest()


def register(username, password):
    if ur.user_exists(username):
        return 'User already exists.'
    try:
        h = __hash(password)
        dirtools.ensure_user_dir(username)
        dirtools.xor_dir(username, h)
        ur.create_user(username, h, dirtools.dir_hash(username))
    except Exception as e:
        return e.message
    return 'User successfully registered. Now you can login.'


def login(username, password):
    h = __hash(password)
    if not ur.user_exists(username):
        return "Wrong username";
    if not ur.check_user(username, h):
        return 'Wrong password.'
    if ur.get_user(username).folder_hash != dirtools.dir_hash(username):
        return "Directory was changed. You can't access your files now. Contact your system administrator"

    dirtools.xor_dir(username, password)
    os.system('cd /home/sntp')
    os.chdir(dirtools.user_dir(username))
    print 'You have successfully logged id. Now you can access your files.'
    while True:
        cmd = raw_input(username + '@PRO$ ')
        if cmd.lower() == 'exit':
            break
        os.system(cmd)
    dirtools.xor_dir(username, password)
    ur.update_hash(username, dirtools.dir_hash(username))
    print 'Logged out.'


def delete(username, password):
    h = __hash(password)
    if not ur.check_user(username, h):
        return 'Wrong username or password.'
    try:
        dirtools.delete(username)
        ur.delete(username)
        return 'User was successfully deleted.'
    except Exception as e:
        return e.message
