#!/usr/bin/python
import os, sys


SIGNATURE_LENGTH = 32


def files(path):
    return ['%s/%s' % (d[0], f) for d in os.walk(path) for f in d[2]]


def signature(filename):
    with open(filename) as f:
        f.seek(os.path.getsize(filename) / 2)
        return f.read(SIGNATURE_LENGTH)


filename = os.path.abspath(sys.argv[1])
path = os.path.abspath(sys.argv[2])
sig = signature(filename)
for f in filter(lambda x: x != filename and signature(x) == sig, files(path)):
    print f

